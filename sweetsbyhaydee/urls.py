from django.conf.urls import url
from . import views

#TEMPLATE TAGGING
app_name = 'sweetsbyhaydee'

urlpatterns = [
    url(r'^users/$', views.users, name='users'),
    url(r'^quem_sou/$', views.quem_sou, name='quem_sou'),
]
