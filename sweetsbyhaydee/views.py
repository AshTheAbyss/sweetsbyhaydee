from django.shortcuts import render
#from django.http import HttpResponse
#from sweetsbyhaydee.models import User
from sweetsbyhaydee.forms import NewUserForm
# Create your views here.

def index(request):
    return render(request, 'sweetsbyhaydee/index.html')

def quem_sou(request):
    return render(request, 'sweetsbyhaydee/quem_sou.html')

def base(request):
    return render(request, 'sweetsbyhaydee/base.html')

def users(request):
    form = NewUserForm()
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return index(request)
        else:
            print('ERROR FORM INVALID')
    return render(request, 'sweetsbyhaydee/users.html', {'form':form})
